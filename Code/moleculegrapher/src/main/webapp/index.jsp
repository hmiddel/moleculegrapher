<%--
  Created by IntelliJ IDEA.
  User: hmiddel
  Date: 4-12-18
  Time: 14:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Graph Parser</title>
    <meta name="description" content="">
    <meta name="author" content="ink, cookbook, recipes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">


    <!-- load Ink's css from the cdn -->
    <link rel="stylesheet" type="text/css"
          href="http://cdn.ink.sapo.pt/3.1.10/css/ink-flex.min.css">
    <link rel="stylesheet" type="text/css"
          href="http://cdn.ink.sapo.pt/3.1.10/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/canvas.css">

    <!-- Load all scripts. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/springy.js"></script>
    <script src="js/springyui.js"></script>
    <script src="js/graph_drawer.js"></script>

    <!-- load Ink's css for IE8 -->
    <!--[if lt IE 9 ]>
    <link rel="stylesheet" href="http://cdn.ink.sapo.pt/3.1.10/css/ink-ie.min.css" type="text/css" media="screen"
          title="no title" charset="utf-8">
    <![endif]-->

    <!-- test browser flexbox support and load legacy grid if unsupported -->
    <script type="text/javascript" src="http://cdn.ink.sapo.pt/3.1.10/js/modernizr.js"></script>
    <script type="text/javascript">
        Modernizr.load({
            test: Modernizr.flexbox,
            nope: 'http://cdn.ink.sapo.pt/3.1.10/css/ink-legacy.min.css'
        });
    </script>

    <!-- load Ink's javascript files from the cdn -->
    <script type="text/javascript" src="http://cdn.ink.sapo.pt/3.1.10/js/holder.js"></script>
    <script type="text/javascript" src="http://cdn.ink.sapo.pt/3.1.10/js/ink-all.min.js"></script>
    <script type="text/javascript" src="http://cdn.ink.sapo.pt/3.1.10/js/autoload.js"></script>

</head>

<body bgcolor="#E6E6FA">
<div id='content' style='width:100%;'>
    <input id="input" class="parser" onkeyup="keyUp()" placeholder="Enter a valid molecule string ..." type="text">
</div>

<div id="errorbox" class="error"></div>

<!-- Buttons for inputting amino acids -->
<div class="aa" style="float:left">
    <H1>Amino Acid Selection</H1>
<div class="button-group" role = "group">
    <button class="ink-button" value="<BB>"  onclick='AA(this)'>&nbsp;BB&nbsp;</button>
    <button class="ink-button" value="<GLY>" onclick='AA(this)'>GLY</button>
    <button class="ink-button" value="<ALA>" onclick='AA(this)'>ALA</button>
    <button class="ink-button" value="<ASP>" onclick='AA(this)'>ASP</button>
    <button class="ink-button" value="<ASN>" onclick='AA(this)'>ASN</button>
    <button class="ink-button" value="<SER>" onclick='AA(this)'>SER</button>
    <button class="ink-button" value="<CYS>" onclick='AA(this)'>CYS</button>
</div>
    <div class="button-group" role = "group">
    <button class="ink-button" value="<MET>" onclick='AA(this)'>MET</button>
    <button class="ink-button" value="<THR>" onclick='AA(this)'>THR</button>
    <button class="ink-button" value="<VAL>" onclick='AA(this)'>VAL</button>
    <button class="ink-button" value="<ILE>" onclick='AA(this)'>ILE</button>
    <button class="ink-button" value="<LEU>" onclick='AA(this)'>LEU</button>
    <button class="ink-button" value="<GLU>" onclick='AA(this)'>GLU</button>
    <button class="ink-button" value="<GLN>" onclick='AA(this)'>GLN</button>
    </div>
    <div class="button-group" role = "group">
    <button class="ink-button" value="<PRO>" onclick='AA(this)'>PRO</button>
    <button class="ink-button" value="<HIS>" onclick='AA(this)'>HIS</button>
    <button class="ink-button" value="<PHE>" onclick='AA(this)'>PHE</button>
    <button class="ink-button" value="<TYR>" onclick='AA(this)'>TYR</button>
    <button class="ink-button" value="<LYS>" onclick='AA(this)'>LYS</button>
    <button class="ink-button" value="<ARG>" onclick='AA(this)'>ARG</button>
    <button class="ink-button" value="<TRP>" onclick='AA(this)'>TRP</button>

    </div>
    <div class="help" style="float:left">
        <details>
            <summary><h4>Help</h4></summary>
            Grappa string Rules:
            <BR>
            <BR>name        : add node with name, with edge to active node
            <BR>(none at start, active parent at start of branch)
            <BR>-name       : remove node with name
            <BR>
            <BR>@name       : select name as active node
            <BR>
            <BR>(           : set active node as active parent (start branching)
            <BR>
            <BR>,           : switch to new branch at active parent
            <BR>
            <BR>)           : set active parent to active node
            <BR>
            <BR>=nameB      : rename active node (keep edges)
            <BR>
            <BR>{attr=val}  : set attribute on active node (can be
            <BR>attributes like:  element, charge, valence, stubs
            <BR>element is set to FIRST LETTER of name,
            <BR>unless specified as attribute
            <BR>attribute chiral has tuple of three nodes,
            <BR>which define chirality according to right-hand rule
            <BR>
            <BR>!X          : connect active node to node X, which _must_ be present
            <BR>already. Otherwise, using a name that is already there is an
            <BR>error
            <BR>
            <BR>&lt;NAME&gt;      : include brick with given name
            <BR>
            <BR>&lt;':NAME&gt;    : include brick with given name and add ' as suffix to nodes
            <BR>
            <BR>&lt;NAME@X&gt;    : include brick with given name and add edge between active
            <BR>node and node 'X' of brick
            <BR>
            <BR>/#=1-20/C#(H#[1-2])/ : Expand by repetion and substitution according to
            <BR>range specified
            <BR>
            <BR>(/#=A-D/C#(H#[1-3]),/) : Expand to multiple branches
        </details>
    </div>
</div>

<!--[if lte IE 9 ]>
<div class="ink-alert basic" role="alert">
    <button class="ink-dismiss">&times;</button>
    <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
    </p>
</div>
-->

<!-- Add your site or application content here -->
<br clear="all"/>
<div>
<canvas id="grappa_canvas" width="800" height="500"></canvas>
</div>


</body>
</html>
