import argparse
import json

from grappa import *


def main(input_string, output_file, ref_values):
    graphs = {}
    if ref_values:
        if len(ref_values) % 2 != 0:
            print(len(ref_values))
            return
        else:
            for i in range(0, int(len(ref_values)), 2):
                prep = preprocess(ref_values[i+1])
                graph = process(prep, graphs=graphs)
                graphs[ref_values[i]] = graph
    prep = preprocess(input_string)
    if graphs:
        graph = process(prep, graphs=graphs)
    else:
        graph = process(prep)
    if output_file:
        with open(output_file, "w") as f:
            json.dump({"nodes": graph.nodes(), "edges": graph.edges()}, f)
    else:
        print(json.dumps({"nodes": graph.nodes(), "edges": graph.edges()}))

parser = argparse.ArgumentParser(description='Testing the argparse')
parser.add_argument('-i', metavar='input_string', dest='input_string', type=str, help='Input string for a molecule')
parser.add_argument('-o', metavar='output_file', dest='output_file', type=str,  help='Output file for JSON')
parser.add_argument('-r', metavar='ref_values', dest='ref_values', type=str,  help='Do not use manually!')
args = parser.parse_args()
if args.ref_values:
    refs = args.ref_values.split("\t")
else:
    refs = None

if __name__ == "__main__":
    main(args.input_string, args.output_file, refs)