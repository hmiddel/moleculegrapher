// Cached JSON. This contains the previous set of JSON.
var CachedJson = "";

function AA(button){
    //When user enters a button, the value is put in the input field, then grappa is run using normal keyUp(); event
    document.getElementById("input").value = button.value;
    keyUp();
}

// A function to find the difference in nodes contained in two json objects.
function jsonDiffNodes(a, b) {
    added = [];
    removed = [];

    // Iterate through all nodes of the first argument, check if they are in the second argument. If not, a node was added.
    for (var i = 0; i < a.nodes.length; i++) {
        if (b.nodes.indexOf(a.nodes[i]) < 0) {
            added.push(a.nodes[i]);
        }
    }

    // Iterate through all nodes of the second argument, check if they are in the first argument. If not, a node was removed.
    for (i = 0; i < b.nodes.length; i++) {
        if (a.nodes.indexOf(b.nodes[i]) < 0) {
            removed.push(b.nodes[i]);
        }
    }
    return [added, removed];
}

// A function to check if an array contains another array.
function containsArray(array_of_arrays, array) {
    for (var j = 0; j < array_of_arrays.length; j++) {
        if (array_of_arrays[j][0] === array[0] && array_of_arrays[j][1] === array[1]) {
            return true;
        }
    }
    return false;
}

// A function to remove all edges
function removeAllEdges() {
    // Remove all with filter
    springy.graph.filterEdges(function(e) {
        return false;
    })
    //}
}

var springy;
var graph;
// Generate an empty graph, for editing later.
jQuery(function () {

    graph = new Springy.Graph();

    var layout = new Springy.Layout.ForceDirected(
        graph,
        4000.0, // Spring stiffness
        1000000.0, // Node repulsion
        0.5 // Damping
    );
    springy = jQuery('#grappa_canvas').springy({graph: graph});


});

// On key up in the input box:
function keyUp() {
    // Get the input
    var data = document.getElementById("input").value;

    //Send a request to the server to process the input with grappa.
    $.post("/index", {"value": data}, function (json_value) {
        var errordiv = document.getElementById("errorbox");
        if (json_value === "error") {
            errordiv.innerHTML = "Warning, invalid syntax!";
        } else {
            errordiv.innerHTML = "";
        }
        // If we have done stuff before
        if (CachedJson !== "" && json_value !== "") {
            // Parse and sort the cached json and the new json.
            var parsed_cache = JSON.parse(CachedJson);
            parsed_cache.nodes.sort();
            for (var i = 0; i < parsed_cache.edges.length; i++) {
                parsed_cache.edges[i].sort();
            }
            parsed_cache.edges.sort();

            var parsed_new = JSON.parse(json_value);
            parsed_new.nodes.sort();
            for (i = 0; i < parsed_new.edges.length; i++) {
                parsed_new.edges[i].sort();
            }
            parsed_new.edges.sort();

            // Get the differences in nodes and edges
            var diff_nodes = jsonDiffNodes(parsed_new, parsed_cache);

            // Get the added nodes and edges, and add them to the graph.
            var added_nodes = diff_nodes[0];
            for (i = 0; i < added_nodes.length; i++) {
                springy.graph.addNodes([added_nodes[i]]);
            }


            var removed_nodes = diff_nodes[1];

            // Remove all edges
            removeAllEdges();

            // Put the edges that should be there back
            setTimeout(function () {
                for (i = 0; i < parsed_new.edges.length; i++) {
                    springy.graph.addEdges([parsed_new.edges[i][0], parsed_new.edges[i][1]]);
                }
            }, 10);

            // Remove the nodes with filter. There is no direct way to remove them with just the id, so we need to check all nodes to see if they are the same as ours.
            for (i = 0; i < removed_nodes.length; i++) {
                springy.graph.filterNodes(function (n) {
                    return !(removed_nodes[i] == n.id);
                })
            }

            // Remove double edges after the new edges have been added
            var edges = [];
            setTimeout(function () {
                springy.graph.filterEdges(function (e) {
                    for (i = 0; i < edges.length; i++) {
                        if ((e.target.id === edges[i].target.id && e.source.id === edges[i].source.id)
                            || (e.source.id === edges[i].target.id && e.target.id === edges[i].source.id)) {
                            //Edge already exists.
                            return false;
                        }
                    }
                    edges.push(e);
                    //alert(JSON.stringify(edges));
                    return true;
                });
            }, 15);

            //We haven't made a graph yet, so make one directly.
        } else if (json_value !== "") {
            graph = graph.loadJSON(json_value);
        }
        // Cache the json.
        CachedJson = json_value;

    });
}