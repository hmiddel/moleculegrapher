package nl.bioinf.moleculegrapher.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * A function to get abbrevations from a grappa string containing references
     * @param input a string containing grappa syntax with references
     * @return an arraylist containing only the abbrevations in the references
     */
    public static ArrayList<String> getReferences(String input){
        ArrayList<String> refs = new ArrayList<>();

        // Find all references (Anything between <>)
        Pattern stripOne = Pattern.compile("<(.*?)>");
        Matcher m = stripOne.matcher(input);
        while (m.find()) {

            // Remove anything before a colon, if it exists.
            String tmp = m.group(1);
            int sliceFront = tmp.indexOf(":");
            if (sliceFront!= -1){
                tmp = tmp.substring(sliceFront+1);
            }

            // Remove anything after an @ symbol, if it exists
            int sliceBack = tmp.indexOf("@");
            if (sliceBack!= -1){
                tmp = tmp.substring(0, sliceBack);
            }

            // Add the stripped down reference, which is now just the abbreviation
            refs.add(tmp);
        }
        return refs;
    }

    /**
     * A function that recieves an array list, then flips it and removes all duplicates from the list
     * This ensures that the arraylist is in dependency order (backbone always first) when the arraylist is parsed
     * @param refGrappaList
     * @returns arraylist cleanrefGrappaList
     */
    public static ArrayList<String> flipAndRemoveDuplicates(ArrayList<String> refGrappaList) {
        ArrayList<String> cleanRefGrappaList = new ArrayList<>();
        Collections.reverse(refGrappaList);
        for (String ref:refGrappaList){
            if(!cleanRefGrappaList.contains(ref)){
                cleanRefGrappaList.add(ref);
            }
        }
        return cleanRefGrappaList;
    }


    public static String parseRefs(ArrayList<String> refs) {
        if (refs.isEmpty()) return "";

        return String.join("\t", refs);
    }
}
