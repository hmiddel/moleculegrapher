package nl.bioinf.moleculegrapher.dao;

import nl.bioinf.moleculegrapher.aminoacids.AminoAcid;

public interface AminoAcidDao {
    /**
     * connects to the data layer.
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    AminoAcid getAminoAcid(String abbrevation) throws DatabaseException;

    void disconnect() throws DatabaseException;
}