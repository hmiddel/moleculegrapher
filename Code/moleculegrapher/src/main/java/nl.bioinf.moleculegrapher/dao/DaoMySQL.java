package nl.bioinf.moleculegrapher.dao;

import nl.bioinf.moleculegrapher.aminoacids.AminoAcid;
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public final class DaoMySQL implements AminoAcidDao {
    private static final String GET_GRAPPA = "get_user";
    Connection connection;
    private Map<String, PreparedStatement> preparedStatements = new HashMap<>();

    /*singleton pattern*/
    private static DaoMySQL uniqueInstance;

    /**
     * singleton pattern
     */
    private DaoMySQL() {}

    /**
     * singleton pattern
     */
    public static DaoMySQL getInstance() {
        //lazy
        if (uniqueInstance == null) {
            uniqueInstance = new DaoMySQL();
        }
        return uniqueInstance;
    }

    @Override
    public void connect() throws DatabaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            DbUser mySQLuser = DbCredentials.getMySQLuser();
            String dbUrl = "jdbc:mysql://" + mySQLuser.getHost() + "/" + mySQLuser.getDatabaseName();
            String dbUser = mySQLuser.getUserName();
            String dbPass = mySQLuser.getDatabasePassword();
            connection = DriverManager.getConnection(dbUrl,dbUser,dbPass);
            prepareStatements();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }
    }

    /**
     * prepares prepared statements for reuse
     * @throws SQLException
     */
    private void prepareStatements() throws SQLException {
        String fetchQuery = "SELECT * FROM aminoacids WHERE abbreviation = ?";
        PreparedStatement ps = connection.prepareStatement(fetchQuery);
        this.preparedStatements.put(GET_GRAPPA, ps);
    }

    @Override
    public AminoAcid getAminoAcid(String abbreviation) throws DatabaseException  {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_GRAPPA);
            ps.setString(1, abbreviation);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String singleLetter = rs.getString("single_letter");
                String fullName = rs.getString("full_name");
                String grappaString = rs.getString("grappa_string");
                AminoAcid AA = new AminoAcid(singleLetter.charAt(0), abbreviation, fullName, grappaString);
                return AA;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
        return null;
    }


    @Override
    public void disconnect() throws DatabaseException {
        try{
            for( String key : this.preparedStatements.keySet() ){
                this.preparedStatements.get(key).close();
            }
        }catch( Exception e ){
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
