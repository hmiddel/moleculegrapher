package nl.bioinf.moleculegrapher.aminoacids;

public class AminoAcid {
    private char singleLetter;
    private String abbrevation;
    private String fullName;
    private String grappaString;

    public AminoAcid(char singleLetter, String abbrevation, String fullName, String grappaString){
        this.singleLetter = singleLetter;
        this.abbrevation = abbrevation;
        this.fullName = fullName;
        this.grappaString = grappaString;
    }

    public char getSingleLetter() {
        return singleLetter;
    }

    public String getAbbrevation() {
        return abbrevation;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGrappaString() {
        return grappaString;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
