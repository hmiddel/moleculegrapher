package nl.bioinf.moleculegrapher.json;


import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * This is a class for processing strings with the grappa python program
 */
public class JsonGetter {
    private final String refs;
    private final String input_string;

    /**
     * @param input_string The string to parse with grappa
     */
    public JsonGetter(final String input_string, final String refs){
        this.input_string = input_string;
        this.refs = refs;

    }


    /**
     * The function that actually calls grappa. Uses input_string as the input for grappa
     * @return a json file as generated by the grappa program, or "error" if grappa errored
     */
    public String callPython(){

        // Get the web-inf directory
        String path = this.getClass().getClassLoader().getResource("").getPath();
        String[] cmd;
        if (!refs.equals("")) {
            cmd = new String[]{
                    "python3",
                    path + "../grappa_runner.py",
                    "-i",
                    this.input_string,
                    "-r",
                    refs,
            };
        } else {
            cmd = new String[]{
                    "python3",
                    path + "../grappa_runner.py",
                    "-i",
                    this.input_string,

            };
        }
        try {
            //Run the script
            Process proc = Runtime.getRuntime().exec(cmd);

            // Get the programs stdout (which is called inputstream)
            InputStream inputstream = proc.getInputStream();
            Scanner s = new Scanner(inputstream).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            inputstream.close();

            // Get the programs stderr
            InputStream errorstream = proc.getErrorStream();
            s = new Scanner(errorstream).useDelimiter("\\A");
            String error = s.hasNext() ? s.next() : "";
            errorstream.close();
            // Return the result only if there were no errors
            if (error.length() == 0) {
                return result;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";
    }
}

