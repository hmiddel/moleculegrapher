package nl.bioinf.moleculegrapher.servlet;

import nl.bioinf.moleculegrapher.utils.StringUtils;
import nl.bioinf.moleculegrapher.aminoacids.AminoAcid;
import nl.bioinf.moleculegrapher.dao.DaoMySQL;
import nl.bioinf.moleculegrapher.dao.DatabaseException;
import nl.bioinf.moleculegrapher.json.JsonGetter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "Servlet", urlPatterns = "/index")
public class Servlet extends javax.servlet.http.HttpServlet {

    private DaoMySQL dao;

    /**
     * Starts up the database connection when program is booted up
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();
        dao = DaoMySQL.getInstance();
        try {
            dao.connect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes database connection when program is shut down
     */
    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main post body of the script, will take input from a website, parse references inside the input.
     * Will then use the input and references to call grappa.py, which returns a list of nodes and edges.
     * These nodes and edges are passed to javascript to draw the molecule
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String data = request.getParameter("value");            // Gets javascript value from input field
        ArrayList<String> refs = StringUtils.getReferences(data);  // Checks input field for references, then finds
        if (!refs.isEmpty()) {                                     // matching grappa strings in database
            refs = getGrappaFromRef(refs);
        }
        JsonGetter json = new JsonGetter(data, StringUtils.parseRefs(refs));
        String graph = json.callPython();                         // Calls grappa.py

        response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(graph);       // Write response body.
    }

    /**
     * Get is not used, it does nothing and is never called. All communication is through the post method.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws
            ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);

    }

    /**
     * A function that recieves a list of references, it then matches these references against a database,
     * gets the corresponding grappa string from the database and returns these in dependency order so grappa.py
     * can use it.
     * @param refs
     * @returns ArrayList
     */
    protected ArrayList<String> getGrappaFromRef(ArrayList<String> refs) {
        ArrayList<String> refGrappaList = new ArrayList<>();

        for (String ref:refs) {                  //matches every ref to database and add to arraylist
            try {
                AminoAcid AA = dao.getAminoAcid(ref);
                refGrappaList.add(AA.getGrappaString());
                refGrappaList.add(ref);
                ArrayList<String> tmpRefs = StringUtils.getReferences(AA.getGrappaString());
                while (!tmpRefs.isEmpty()) {     //checks if ref strings contain further refs and matches those too
                    for (String a: tmpRefs) {
                        AminoAcid AA2 = dao.getAminoAcid(a);
                            refGrappaList.add(AA2.getGrappaString());
                            refGrappaList.add(a);
                        tmpRefs = StringUtils.getReferences(AA2.getGrappaString());
                    }
                }

            } catch (DatabaseException e) {
                System.err.println(e.getMessage());
            }
        }
        ArrayList<String> cleanRefGrappaList = StringUtils.flipAndRemoveDuplicates(refGrappaList);
        return cleanRefGrappaList;
    }

}