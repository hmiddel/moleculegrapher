drop table if exists aminoacids;

create table aminoacids
(   single_letter           char     not null,
    abbreviation             varchar(3)    not null,
    full_name               varchar(50) not null,
    grappa_string           varchar(300),
    primary key(single_letter)
);

insert into aminoacids values ("B", "BB", "Backbone", 'N(H) CA(HA) C(O1) @CA');
insert into aminoacids values ("G", "GLY", "Glycine", '<BB> @HA =HA1 @CA HA2');
insert into aminoacids values ("A", "ALA", "Alanine", '<BB> @CA CB(HB[1-3])');
insert into aminoacids values ("D", "ASP", "AsparticAcid", '<BB> @CA CB(HB1,HB2) CG(OD1,OD2)');
insert into aminoacids values ("N", "ASN", "Asparagine", '<BB> @CA CB(HB1,HB2) CG(OD1) ND2(HD21,HD22)');
insert into aminoacids values ("S", "SER", "Serine", '<BB> @CA CB(HB1,HB2) OG HG');
insert into aminoacids values ("C", "CYS", "Cysteine", '<BB> @CA CB(HB1,HB2) SG HG');
insert into aminoacids values ("M", "MET", "Methionine", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(HD1,HD2) SD CE(HE[1-3])');
insert into aminoacids values ("T", "THR", "Threonine", '<BB> @CA CB (OG1 HG1) CG2 (HG2[1-3])');
insert into aminoacids values ("V", "VAL", "Valine", '<BB> @CA CB(HB,CG1(HG1[1-3]),CG2(HG2[1-3]))');
insert into aminoacids values ("I", "ILE", "Isoleucine", '<BB> @CA CB(HB,CG2(HG2[1-3])) CG1(HG1[1-2]) CD(HD[1-3])');
insert into aminoacids values ("L", "LEU", "Leucine", '<BB> @CA CB(HB1,HB2) CG(HG1,CD1(HD1[1-3]),CD2(HD2[1-3]))');
insert into aminoacids values ("E", "GLU", "GlutamicAcid", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(OE1,OE2)');
insert into aminoacids values ("Q", "GLN", "Glutamine", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(OE1) NE2(HE21,HE22)');
insert into aminoacids values ("P", "PRO", "Proline", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(HD1,HD2) !C');
insert into aminoacids values ("H", "HIS", "Histidine", '<BB> @CA CB(HB1,HB2) CG CE1(HE1) ND1 CE1(HE1) NE2(HE2) CD2(HD2) !CG');
insert into aminoacids values ("F", "PHE", "Phenylalanine", '<BB> @CA CB(HB1,HB2) CG CD1(HD1) CE1(HE1) CZ(HZ) CE2(HE2) CD2(HD2) !CG');
insert into aminoacids values ("Y", "TYR", "Tyrosine", '<PHE> -HZ @CZ OH HH');
insert into aminoacids values ("K", "LYS", "Lysine", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(HD1,HD2) CE(HE1,HE2) NZ(HZ1,HZ2,HZ3)');
insert into aminoacids values ("R", "ARG", "Arginine", '<BB> @CA CB(HB1,HB2) CG(HG1,HG2) CD(HD1,HD2) NE(HE) CZ(NH1(HH11,HH12),NH2(HH21,HH22))');
insert into aminoacids values ("W", "TRP", "Tryptophan", '<BB> @CA CB(HB1,HB2) CG CD1(HD1) NE1(HE1) CE2 CZ2(HZ2) CH2(HH2) CZ3(HZ3) CE3(HE3) CD2 !CG @CD2 !CE2');



