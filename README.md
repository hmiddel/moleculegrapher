CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation and use
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

This project contains a web app for interactive use of the grappy python script. This is a script that generates graphs based on a specific syntax.
 
These graphs are shown dynamically (e.g. while typing) on the website.

REQUIREMENTS
------------

*Java version 8 or higher

*Springy version 2.7 or higher (bundled with the program as springy.js)

*MySQL

*A .my.cnf file containing valid database credentials in the home directory

*Tomcat version 9.0.13 or higher 

INSTALLATION AND USE
------------------------

Build the program as a .war file. Run the provided mysql script (aminoacids.sql). Run the war file using tomcat.

Documentation on the grappa syntax can be found on the webpage after installation, or in grappa.py in the WEB-INF directory.

CONFIGURATION
-------------

No configuration changes can be made to the program.

TROUBLESHOOTING
---------------

If you are receiving invalid syntax errors while using prebuilt bricks, check if the database contains these bricks in the abbreviation field.